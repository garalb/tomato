**TOMATO** (**To**y **Mat**rix **O**n-The-Fly)

**ABOUT**

  * TOMATO generates realistic Hamiltonian and overlap matrices to use as test cases for Kohn-Sham ground-state solvers.

  * This repo contains data files used by TOMATO (toy matrices on-the-fly) to generate test matrices for ELSI (ELectronic Structure Infrastructure, www.elsi-interchange.org). ELSI provides a unified software interface to simplify the access to existing strategies to solve or circumvent the Kohn-Sham eigenvalue problems in the self-consistent field cycle of density-functional theory.

**USAGE**

  * Data files in this repo can be used to test ELSI. Please follow the instruction in ELSI package.

**CONTACT**

  * Email: elsi-team@duke.edu

  * Homepage: www.elsi-interchange.org
